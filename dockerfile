#base image 
FROM openjdk:8
COPY . /src/java
WORKDIR /src/java
EXPOSE 5000 
RUN ["javac", "hello.java"]
ENTRYPOINT ["java","hello"]
